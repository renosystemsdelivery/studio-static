$(document).ready(function() {

    $(".players-header .btn-blue").hover(function() {
        $(".players-header .filter-icon").attr("src", "images/filter-white.png");
    });
    $(".players-header .btn-blue").focus(function() {
        $(".players-header .filter-icon").attr("src", "images/filter-white.png");
    });
    $(".players-header .btn-blue").mouseout(function() {
        $(".players-header .filter-icon").attr("src", "images/filter.png");
    });

    /* Alerts */
    $(".delete-area").click(function() {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            showCancelButton: true,
            confirmButtonColor: '#FF7302',
            confirmButtonText: 'Yes, Delete it!'
        })
        .then((result) => {
            if (result.value) {
              Swal.fire(
                'Deleted!',
                'The Row has been deleted.',
                'success'
                )
            }
        })
    });

    $(".procced").click(function() {
        Swal.fire({
            title:'FAILURE',
            text: 'Validation error occurred, please review sheet again',
            confirmButtonText: 'Done',
            icon: 'error'
        })
    });
    $(".procced").click(function() {
        Swal.fire({
            title:'SUCCESS',
            text: 'Data validatedand 300 players successfully uploaded',
            confirmButtonText: 'Done',
            icon: 'success'
        })
    });

    $(".success").click(function() {
        Swal.fire({
            title:'SUCCESS',
            text: 'Your Transaction Is successfully done',
            confirmButtonText: 'Done',
            icon: 'success'
        })
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.toast').toast();
    setTimeout('$(".failure .toast").hide()', 3000);
    setTimeout('$(".success .toast").hide()', 2000);

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(".more").click(function(){
        $(".another-lvl .hide").toggleClass('show');
        $(this).text($(this).text() == 'Show More Levels' ? 'Show Less' : 'Show More Levels');
    })

    $(".card .tooltip-img").click(function(){
        $(this).parent().parent().find(".card-info-layover").removeClass("d-none");
    })
    $(".card .close-btn").click(function(){
        $(this).parent().addClass("d-none");
    })

});