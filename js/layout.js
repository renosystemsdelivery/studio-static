$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("w-sidebar");
    $(".list-group-item-action .title").toggleClass("hide");
    $(".list-item").toggleClass("w-25");
    $(".sidebar-heading").toggleClass("hide");
    $("#sidebar-wrapper .head-of-sidebar").toggleClass("justify-content-center");
    $("#sidebar-wrapper .head-of-sidebar").toggleClass("mt-3");
    $("#sidebar-wrapper .list-item").toggleClass("d-block");
    $("#sidebar-wrapper .list-group").toggleClass("mt-16");
    $("#sidebar-wrapper .list-item").toggleClass("p-7");
  });